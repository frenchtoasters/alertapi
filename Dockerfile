FROM python:3.7

EXPOSE 5000

# Copy Dependices
WORKDIR /opt/Project/
COPY . /opt/Project/

# Installing Pip requirements
RUN pip install -r /opt/Project/requirements.txt

# since we will be "always" mounting the volume, we can set this up
ENTRYPOINT ["python"]
CMD ["api/api.py"]


