from flask import Flask, request
import json
import sys
app = Flask(__name__)
 
@app.route("/test")
def hello():
    content = {"Message": "Result"}
    return json.dumps(content)

def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError("Not running with the Werkzeug Server")
    func()

@app.route("/shutdown")
def shutdown():
    shutdown_server()
    return "Turning off server..."
 
if __name__ == "__main__":
    app.run(host='0.0.0.0')
